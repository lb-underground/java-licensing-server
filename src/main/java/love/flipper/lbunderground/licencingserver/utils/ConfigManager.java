package love.flipper.lbunderground.licencingserver.utils;

import love.flipper.lbunderground.licencingserver.LBUnderground;
import love.flipper.lbunderground.licencingserver.utils.obj.Config;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class ConfigManager {

    private LBUnderground lbUnderground;
    private Config config;
    private final File configFolder, configFile;

    //config values
    private ArrayList<String> lbMasterServerAddresses;
    private int port;
    private String magic;

    public ConfigManager(LBUnderground instance) {
        lbUnderground = instance;
        configFolder = new File(System.getenv("APPDATA") + "\\" + lbUnderground.getProgramName().replaceAll(" ", ""));
        configFile = new File(configFolder, "config.json");
        readConfigFiles();
    }

    public void readConfigFiles() {
        try {
            if(!configFolder.exists() || !configFile.exists()) {
                if(!configFolder.exists()) configFolder.mkdirs();
                configFile.createNewFile();
                lbUnderground.logMessage("Default config was created at " + configFile.getPath());
                createDefaultConfig
            }
            lbUnderground.logMessage("Loading config from " + configFile.getPath());
            this.config = new ObjectMapper().readValue(configFile, Config.class);
            this.lbMasterServerAddresses = new ArrayList<>(){{add(config.getOverrideServerAddress());}};
            this.port = config.getPort();
            this.magic = config.getMagic();
            return;
        } catch (Exception e){
            e.printStackTrace();
            // TODO: UNDERGROUND-00602
            lbUnderground.logMessage("Failed to map! Assuming the json file is malformed. UNDERGROUND-00602");
            configFile.deleteOnExit();
            Runtime.getRuntime().exit(0);
        }
    }

    /**
     * @throws IOException
     * Populates config with default values.
     */
    private void createDefaultConfig() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Config config = new Config("1337", "213.109.160.200", 5001);
        objectMapper.writeValue(configFile, config);
    }

    // Getters & Setters


    public File getConfigFolder() {
        return configFolder;
    }

    public File getConfigFile() {
        return configFile;
    }

    public ArrayList<String> getLbMasterServerAddresses() {
        return lbMasterServerAddresses;
    }

    public int getPort() {
        return port;
    }

    public String getMagic() {
        return magic;
    }
}
