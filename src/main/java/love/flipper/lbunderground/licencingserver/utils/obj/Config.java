package love.flipper.lbunderground.licencingserver.utils.obj;

public class Config {

    private String magic, overrideServerAddress;
    private int port;

    public Config(String magic, String overrideServerAddress, int port) {
        this.magic = magic;
        this.overrideServerAddress = overrideServerAddress;
        this.port = port;
    }

    /**
     * Dummy constructor for Jackson library.
     * Don't know why this is needed.
     */
    public Config() {

    }

    public String getMagic() {
        return magic;
    }

    public String getOverrideServerAddress() {
        return overrideServerAddress;
    }

    public int getPort() {
        return port;
    }
}
