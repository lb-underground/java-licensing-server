package love.flipper.lbunderground.licencingserver;

import love.flipper.lbunderground.licencingserver.http.LicencingServer;
import love.flipper.lbunderground.licencingserver.utils.ConfigManager;

import java.net.InetAddress;
import java.util.ArrayList;

public class LBUnderground {

    private static LBUnderground instance;
    public LicencingServer licencingServer;
    public final ConfigManager configManager;

    private final String programName = "LB Underground";
    private final String version = "v1.2.1";
    public String logPrefix = "[" + programName + "] ";

    //TODO: Handle arguments
    public LBUnderground(String[] args) {
        instance = this;
        configManager = new ConfigManager(instance);
        setup();
    }

    private void setup() {
        logMessage("Checking to see if LBMasters servers are correctly mapped to localhost.");
        if(!(areServersRoutedCorrectly())) {
            logMessage("Servers are NOT routed correctly! Routing them now.");
            routeServers();
            setup();
        } else logMessage("Servers are routed correctly!");

        //TODO: Add support for more than one IP if possible
        // Start Licencing Server
        this.licencingServer = new LicencingServer(configManager.getLbMasterServerAddresses().get(0), configManager.getPort());
    }

    /**
     * @return Routes all ips inside lbMasterServerAddresses to
     *         localhost/loopback address.
     */
    private boolean routeServers() {
        try {
            logMessage("Please click yes on the following admin prompts. (If any appear)");
            for(String address : configManager.getLbMasterServerAddresses()) {
                logMessage("Mapping " + address + " to localhost.");
                String command = "netsh int ip add addr 1 " + address + "/32 st=ac sk=tr";
                logMessage("Running " + command);
                Runtime.getRuntime().exec(command);

            }
            if(!areServersRoutedCorrectly()) {
                Runtime.getRuntime().exit(0);
            }
            return true;
        } catch (Exception e) {
            // TODO: UNDERGROUND-00002
            logMessage("Failed to get admin privileges. Try running the commands above! UNDERGROUND-00002");
            Runtime.getRuntime().exit(0);
            return false;
        }
    }

    /**
     * @return Checks to see if LBMasters' servers are
     *         correctly set to localhost.
     */
    private boolean areServersRoutedCorrectly() {
        ArrayList<String> addrSpace = new ArrayList<>();
        for(String host : configManager.getLbMasterServerAddresses()) {
            try {
                // TODO: UNDERGROUND-00001
                // Dodgy way of seeing if his servers point to the local network
                if(InetAddress.getByName(host).isReachable(1/10)) {
                    addrSpace.add(host);
                    logMessage(host + " is a loopback address.");
                }
            } catch (Exception e) {
                e.printStackTrace();
                logMessage("Error while checking if " + host + " is a loopback address. Error code: UNDERGROUND-00001 ");
            }
        }
        return configManager.getLbMasterServerAddresses().toString().contentEquals(addrSpace.toString());
    }

    public void logMessage(Object msg) {
        System.out.println(logPrefix + msg);
    }

    public String getProgramName() {
        return programName;
    }

    public static LBUnderground getInstance() {
        return instance;
    }

    public String getVersion() {
        return version;
    }

}
