package love.flipper.lbunderground.licencingserver.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import love.flipper.lbunderground.licencingserver.LBUnderground;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

public class LBHttpHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        OutputStream outputStream = exchange.getResponseBody();
        if(!exchange.getRequestHeaders().containsKey("User-Agent")) {
            LBUnderground.getInstance().logMessage(exchange.getRemoteAddress() + " Requested " + exchange.getRequestURI() + " from DayZ.");
            String htmlResponse = LBUnderground.getInstance().configManager.getMagic();
            exchange.sendResponseHeaders(200, htmlResponse.length());
            outputStream.write(htmlResponse.getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
        } else {
            LBUnderground.getInstance().logMessage(exchange.getRemoteAddress() + " Requested " + exchange.getRequestURI() + " from browser");
            StringBuilder htmlBuilder = new StringBuilder();
            htmlBuilder.append("<html><head><title>LB Underground</title></head><body><h2>LB Underground</h2><h3> Made by Flipper#3241 for use on Risen servers</h3><hr>LB Underground | Version: ").append(LBUnderground.getInstance().getVersion()).append("</hr></body></html>");
            exchange.sendResponseHeaders(200, htmlBuilder.toString().length());
            outputStream.write(htmlBuilder.toString().getBytes());
            outputStream.flush();
            outputStream.close();
        }
    }
}
