package love.flipper.lbunderground.licencingserver.http;

import com.sun.net.httpserver.HttpServer;
import love.flipper.lbunderground.licencingserver.LBUnderground;

import java.net.InetSocketAddress;

public class LicencingServer {

    private HttpServer httpServer;

    public LicencingServer(String hostname, int port) {
        try {
            httpServer = HttpServer.create(new InetSocketAddress(hostname, port), 0);
            httpServer.createContext("/", new LBHttpHandler());
            httpServer.start();
            LBUnderground.getInstance().logMessage("Server has started on http://" + hostname + ":" + port + "/");
        } catch (Exception e) {
            // TODO: UNDERGROUND-00006
            LBUnderground.getInstance().logMessage("Failed to create HTTP server! UNDERGROUND-00006");
            Runtime.getRuntime().exit(0);
        }

    }
}
