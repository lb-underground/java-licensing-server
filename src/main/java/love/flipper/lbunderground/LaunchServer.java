package love.flipper.lbunderground;

import love.flipper.lbunderground.licencingserver.LBUnderground;

public class LaunchServer {
    public static void main(String[] args) {
        new LBUnderground(args);
    }
}
